import React, { Component } from 'react';

class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            address: "",
            phone_number: ""
        }
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }


    render() {
        return (
            <React.Fragment>
                <div className="container">
                    <div className="row">
                        <h1 align="center">Person Details</h1>
                    </div>

                    <br />

                    <div className="row">
                        <label>Name:</label>
                        <input type="text" name="name" value={this.state.name} onChange={this.handleChange} className="form-control" placeholder="Enter your Name" />
                    </div>

                    <br />

                    <div className="row">
                        <label>Email:</label>
                        <input type="text" name="email" value={this.state.email} onChange={this.handleChange} className="form-control" placeholder="Enter your E-mail ID" />
                    </div>

                    <br />

                    <div className="row">
                        <label>Address:</label>
                        <input type="text" name="address" value={this.state.address} onChange={this.handleChange} className="form-control" placeholder="Enter your Address" />
                    </div>

                    <br />

                    <div className="row">
                        <label>Phone Number:</label>
                        <input type="text" name="phone_number" value={this.state.phone_number} onChange={this.handleChange} className="form-control" placeholder="Enter your Phone Number" />
                    </div>

                    <br />

                    <div className="row">
                        <button type="submit" onClick={() => {
                            return (
                                this.props.handleFormChange(this.state)
                            )
                        }} className="btn btn-success">SUBMIT</button>
                    </div>

                </div>
            </React.Fragment>
        );
    }
}

export default Form;