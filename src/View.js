import React, { Component } from 'react';

class View extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }


    render() {
        let persondata = this.props.persondata;
        return (
            <div className="container">
                <div className="row">
                    <table className="table table-primary">

                        <thead>
                            <tr>
                                <th>Person ID</th>
                                <th>Name</th>
                                <th>E-mail ID</th>
                                <th>Address</th>
                                <th>Phone Number</th>
                            </tr>
                        </thead>

                        <tbody>

                            {persondata.map(function (data, index) {
                                if (data.address === "Raia") {
                                    return (
                                        <tr key={index} className="bg-danger">
                                            <td>{index + 1}</td>
                                            <td>{data.name}</td>
                                            <td>{data.email}</td>
                                            <td>{data.address}</td>
                                            <td>{data.phone_number}</td>
                                        </tr>
                                    )
                                }
                                else {
                                    return (
                                        <tr key={index}>
                                            <td>{index + 1}</td>
                                            <td>{data.name}</td>
                                            <td>{data.email}</td>
                                            <td>{data.address}</td>
                                            <td>{data.phone_number}</td>
                                        </tr>
                                    )
                                }
                            })
                            }
                        </tbody>

                    </table>
                </div>
            </div>
        );
    }
}

export default View;