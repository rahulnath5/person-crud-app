import React, { Component } from 'react';
import Form from './Form';
import View from './View';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            persondata: [],
            name: "",
            email: "",
            address: "",
            phone_number: ""
        }
        this.handleFormChange = this.handleFormChange.bind(this);
    }


    handleFormChange(data) {
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", "http://localhost:8060/");
        xmlhttp.setRequestHeader("Content-Type", "application/json");
        xmlhttp.send(JSON.stringify(data));

        let person_array = [...this.state.persondata];
        person_array.push(data);
        this.setState({ persondata: person_array });



        console.log(data);
    }

    componentDidMount() {
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", "http://localhost:8060/");
        xmlhttp.send();
        xmlhttp.onload = function () {
            let serverData = xmlhttp.responseText;
            let data = JSON.parse(serverData);
            console.log(serverData);
            this.setState({ persondata: data });
            console.log(this.state.persondata);
        }.bind(this);
    }


    render() {
        return (
            <React.Fragment>

                <div className="container-fluid">
                    <br />
                    <div className="row">
                        <Form name={this.props.name}
                            email={this.props.email}
                            address={this.props.address}
                            phone_number={this.props.phone_number}
                            handleFormChange={this.handleFormChange} />
                    </div>

                    <br />

                    <div className="row">
                        <View persondata={this.state.persondata} />
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default App;